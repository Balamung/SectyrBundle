<?php

namespace SectyrBundle\Controller\Sectyr;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @Route("/")
 */
class TurtleController extends Controller
{
    private $separator = "--------------------------\n";

    /**
     * @Route("/operations", name="sectyr_turtle")
     */
    public function indexAction()
    {
        return $this->render('SectyrBundle:Sectyr:turtle.html.twig');
    }


    /**
     * @Route("/operations/command", name="sectyr_turtle_command")
     * @Method({"POST"})
     */
    public function turtleCommandAction(Request $request)
    {
        if($request->request->has('command')){
            $message = urldecode(strtolower(str_replace('%20', ' ', $request->request->get('command'))));
        } else {
            return new NotFoundHttpException();
        }

        $turtleData = $this->getParameter('turtle');

        if(in_array($message, array_keys($turtleData['hidden_codes']))){
            return new JsonResponse([
                'response' =>  $turtleData['hidden_codes'][$message]['message'] . "\n",
                'type'     => 'text'
            ]);
        }

        $params = explode(' ', urldecode($message));

        if($params[0] == "help"){
            return new JsonResponse([
                'response' => $turtleData['help']['text'],
                'type'     => 'text'
            ]);
        } elseif($params[0] == "ops"){
            return $this->opsCommand($params);
        } elseif($params[0][0] == "#" && ctype_alnum(substr($params[0], 1))) {
            $queryCode = substr($params[0], 1);
            foreach($turtleData['operations'] as $operation){
                if(array_key_exists('intel', $operation) && in_array($queryCode, array_keys($operation['intel']))){
                    return new JsonResponse([
                        'response' =>  $operation['intel'][$queryCode]['link'] . "\n",
                        'type'     => 'link'
                    ]);
                }
            }

            return new JsonResponse([
                'response' => "Intel code not found\n",
                'type'     => 'text'
            ]);
        }

        if($params[0] == "discord"){
            return new JsonResponse([
                'response' => "https://discordapp.com/invite/P3N7vyT\n",
                'type'     => 'link'
            ]);
        }

        if($params[0] == "forums"){
            return new JsonResponse([
                'response' => "https://forums.aliceandsmith.com/\n",
                'type'     => 'link'
            ]);
        }

        return new JsonResponse([
            'response' => 'Unrecognized command',
            'type'     => 'text'
        ]);
    }

    private function opsCommand($params)
    {
        $turtleData = $this->getParameter('turtle');

        if ($params[1] == 'list' || $params[1] == 'active') {
            $all = $params[1] == 'list';
            $response = $this->separator . "List of Operations\n" . $this->separator;
            foreach ($turtleData['operations'] as $op) {
                if ($all || $op['status'] == "ACTIVE") {
                    $response .= $op['name'] . " - " . $this->echoStatus($op['status']) . "\n";
                }
            }

            return new JsonResponse([
                'response' => $response,
                'type' => 'text'
            ]);
        } elseif (in_array($params[1], array_keys($turtleData['operations']))) {
            $operation = $turtleData['operations'][$params[1]];
            if (count($params) == 2) {
                $response = $this->separator . "Operation " . $operation['name'] . "\n" . $this->separator;
                if (array_key_exists('status', $operation))
                    $response .= "Status : " . $this->echoStatus($operation['status']) . "\n";
                if (array_key_exists('brief', $operation))
                    $response .= "Brief : " . $operation['brief'] . "\n";
                if (array_key_exists('intel', $operation)) {
                    $response .= $this->separator;
                    $response .= "Intel documents :\n";
                    foreach ($operation['intel'] as $key => $intel) {
                        $response .= "[[b;purple;black]" . strtoupper($key) . "] > " . $intel['name'] . "\n";
                    }
                }

                return new JsonResponse([
                    'response' => $response,
                    'type' => 'text'
                ]);
            } elseif (count($params) == 3) {
                if (array_key_exists('submits', $operation)) {
                    if(array_key_exists($params[2], $operation['submits'])){
                        return new JsonResponse([
                            'response' => $operation['submits'][$params[2]]['message'] . "\n",
                            'type' => $operation['submits'][$params[2]]['type']
                        ]);
                    }

                    if(array_key_exists('all', $operation['submits'])){
                        return new JsonResponse([
                            'response' => $operation['submits']['all']['message'] . "\n",
                            'type' => $operation['submits']['all']['type']
                        ]);
                    }

                    return new JsonResponse([
                        'response' => 'Intel not recognized for the specified operation' . "\n",
                        'type' => 'text'
                    ]);
                }
            }
        } else {
            return new JsonResponse([
                'response' => "Operation not found\n",
                'type'     => 'text'
            ]);
        }
        return new JsonResponse([
            'response' => "Invalid command\n",
            'type'     => 'text'
        ]);
    }

    private function echoStatus($status){
        switch($status){
            case "ACTIVE":
                return "[[b;green;black]$status]";
            case "COMPLETED":
                return "[[b;darkred;black]$status]";
            default:
                return $status;
        }
    }
}
