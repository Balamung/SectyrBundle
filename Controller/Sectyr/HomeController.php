<?php

namespace SectyrBundle\Controller\Sectyr;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Route("/")
 */
class HomeController extends Controller
{
    /**
     * @Route("/", name="pax_knight")
     */
    public function paxAction()
    {
        return $this->render('SectyrBundle:Sectyr:knight.html.twig');
    }

    /**
     * @Route("/pax/message", name="pax_chat_message", options = { "expose" = true })
     * @Method({"POST"})
     */
    public function paxChatMessageResponse(Request $request)
    {
        if($request->request->has('message')){
            $message = str_replace('%20', ' ', strtolower($request->request->get('message')));
        } else {
            return new NotFoundHttpException();
        }


        $triggers = [
            'i like trains'            => ['link', $request->getUriForPath('/documents/test_donors.pdf')],
            'i am ready to ascend'     => ['message', 'Welcome recruit, please introduce yourself to your fellow agents here : https://discordapp.com/invite/P3N7vyT'],
            'turtle'                   => ['slink', $this->generateUrl('sectyr_turtle')],
            'slink'                    => ['slink', 'http://blackwatchmen.com/']
        ];


        if(in_array($message, array_keys($triggers))) {
            return new JsonResponse([
                'response' => $triggers[strtolower($message)][1],
                'cs'       => '',
                'type'     => $triggers[strtolower($message)][0]
            ]);
        }

        /*return new JsonResponse([
            'response' => "I am broke, come back later =(",
            'cs'       => ''
        ]);*/

        $key = $this->getParameter('cleverbot_api_key');

        $url = 'http://www.cleverbot.com/getreply?key='.$key.'&input='.$message;

        if($request->request->has('cs') && !empty($request->request->get('cs'))){
            $url .= "&cs=" . $request->request->get('cs');
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);
        $cleverbotResponse = json_decode($output);

        return new JsonResponse([
            'response' => $cleverbotResponse->output,
            'cs'       => $cleverbotResponse->cs,
            'type'     => $message[0]
        ]);
    }
}
